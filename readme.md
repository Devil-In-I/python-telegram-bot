# python-telegram-bot

python-telegram-bot is a sample bot made for fun (at least for now).

## Prerequisites

1. Create the ".env" file.
2. Paste "BOT_TOKEN={Your bot token}" line in there.
3. Paste "API_TOKEN={Your weather api token}" there as well.
4. In order to get this API_KEY, head to https://home.openweathermap.org/users/sign_up link, create a new account(or use existing one, if any) and get your token there from page https://home.openweathermap.org/api_keys


## Installing pip dependencies
```bash
pip install pipreqs
pipreqs --encoding utf-8
pip install -r requirements.txt
```
## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update (add) tests as appropriate.

## License

None