import logging
from logging.handlers import RotatingFileHandler
import os
from dotenv import load_dotenv

load_dotenv()

# Bot token @BotFather
BOT_TOKEN =  os.getenv("BOT_TOKEN", "")

# Your API KEY from https://home.openweathermap.org/api_keys
API_KEY = os.getenv("API_KEY", "")

# Database
DB_URI = os.getenv("DB_URL", "")
DB_NAME = os.getenv("DB_NAME", "")

# Start message
START_MESSAGE = os.getenv("START_MESSAGE", "Hey, folks")

# Logging
LOG_FILE_NAME = "bot_logs.log"

logging.basicConfig(
    level=logging.INFO,
    format="[%(asctime)s - %(levelname)s] - %(name)s - %(message)s",
    datefmt='%d-%b-%y %H:%M:%S',
    handlers=[
        RotatingFileHandler(
            LOG_FILE_NAME,
            maxBytes=50000000,
            backupCount=10
        ),
        logging.StreamHandler()
    ]
)
logging.getLogger("pyrogram").setLevel(logging.WARNING)


def LOGGER(name: str) -> logging.Logger:
    return logging.getLogger(name)