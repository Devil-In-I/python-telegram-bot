import logging
import telebot
import requests
from config import BOT_TOKEN, API_KEY, LOGGER

bot = telebot.TeleBot(BOT_TOKEN)

LOGGER(__name__).info("TeleBot has been created.\n")

#region Handlers

@bot.message_handler(commands=['start', 'hello'])
def send_welcome(message):
    bot.reply_to(message, "Howdy, how are you doing?")

@bot.message_handler(commands=["weather", "/weather"])
def weather_command_handler(message):
    ask_city(message)

@bot.message_handler(func=lambda message: message.text.lower() == 'погода')
def ask_city(message):
    chat_id = message.chat.id
    bot.reply_to(message, "В каком городе вы хотите узнать погоду?")
    bot.register_next_step_handler(message, get_weather)

def get_weather(message):
    chat_id = message.chat.id
    city = message.text.lower()
    url = f'http://api.openweathermap.org/data/2.5/weather?q={city}&appid={API_KEY}&lang=ru&units=metric'    
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        temp = data['main']['temp']
        desc = data['weather'][0]['description']
        wind_speed = data ["wind"]["speed"]
        bot.reply_to(message, f'Температура: {temp} C\nОписание: {desc}\nВетер: {wind_speed}')
    else:
        LOGGER(__name__).error(f'Error while fetching weather data: {response.json()}')




#endregion

LOGGER(__name__).info("Bot has just started listening.")
bot.infinity_polling()

